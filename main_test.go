package main

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHelloWorldHandler(t *testing.T) {
	t.Parallel()
	s := httptest.NewServer(http.HandlerFunc(helloWorld))

	req, err := http.NewRequest(http.MethodGet, s.URL, nil)
	if err != nil {
		t.Fatal(err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}
	want := "Hello, world!\n"
	if string(body) != want {
		t.Errorf("Unexpected body returned. Want %q, got %q", want, string(body))
	}
}

func TestHelloWorldHandler2(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/", nil)

	recorder := httptest.NewRecorder()

	helloWorld(recorder, req)

	resp := recorder.Result()
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}
	want := "Hello, world!\n"
	if string(body) != want {
		t.Errorf("Unexpected body returned. Want %q, got %q", want, string(body))
	}
}
